﻿"use strict";

angular.module("myApp.suggestions", ["ngRoute"])

.config(["$routeProvider", function ($routeProvider) {
    $routeProvider.when("/suggestions", {
        templateUrl: "app/suggestions/suggestions.html",
        controller: "SuggestionsCtrl"
    });
}])
.service('ngservice', function ($http) {
    this.getSearchCities = function (value) {
        var res;        
        if (value.length === 0) {
            res  = $http.get("/Suggestions");
            return res;
        } else if (value.length >= 3) {
            //Lancer la recherche lorsque 3 caractère seulement.
            res = $http.get("/Suggestions?q=" + value);
            return res;
        }
    };
})
.controller('SuggestionsCtrl', ['$scope', 'ngservice', function ($scope, ngservice) {
    $scope.filterValue = "";
    $scope.getSearchCities = function () {
        var promise = ngservice.getSearchCities($scope.filterValue);
        promise.then(function (response) {
            $scope.Cities = response.data;
            $scope.Message = "";
        }, function (err) {
            $scope.Cities = angular.copy($scope.default);
            $scope.Message = "Aucune suggestions";
        });
    };
}]);