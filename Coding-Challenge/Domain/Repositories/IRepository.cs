﻿
using System;
using System.Linq;

namespace Coding_Challenge.Repositories
{
    public interface IRepository
    {
        void Create(object obj);
        void Update(object obj);
        void Delete(object obj);
        object GetById(Type objType, object objId);
        IQueryable<TEntity> GetAll<TEntity>();
    }
}