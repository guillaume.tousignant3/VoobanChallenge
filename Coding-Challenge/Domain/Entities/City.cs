﻿namespace Coding_Challenge.Entities
{
    public class City

    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual double Lat { get; set; }
        public virtual double Long { get; set; }
        public virtual string Country { get; set; }
        public virtual string StateProv { get; set; }
        public virtual double Score { get; set; }

    }

}