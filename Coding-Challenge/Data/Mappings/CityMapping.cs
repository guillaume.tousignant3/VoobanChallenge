﻿using Coding_Challenge.Entities;
using FluentNHibernate.Mapping;


namespace Coding_Challenge.Data.Mappings
{
    public class CityMapping : ClassMap<City>

    {
        public CityMapping()

        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.Lat);
            Map(x => x.Long);
            Map(x => x.StateProv);
            Map(x => x.Country);
            Table("Cities");
        }
    }
}