﻿using Coding_Challenge.Repositories;
using NHibernate;
using NHibernate.Linq;
using System;
using System.Linq;

public class Repository : IRepository, IDisposable
{
    protected ISession _session = null;
    protected ITransaction _transaction = null;
    public Repository()
    {
        _session = Database.OpenSession();
    }
    public Repository(ISession session)
    {
        _session = session;
    }
    public void BeginTransaction()
    {
        _transaction = _session.BeginTransaction();
    }
    public void CommitTransaction()
    {
        _transaction.Commit();
        CloseTransaction();
    }
    public void RollbackTransaction()
    {
        _transaction.Rollback();
        CloseTransaction();
        CloseSession();
    }
    private void CloseTransaction()
    {
        _transaction.Dispose();
        _transaction = null;
    }
    private void CloseSession()
    {
        _session.Close();
        _session.Dispose();
        _session = null;
    }
    public virtual void Create(object obj)
    {
        _session.Save(obj);
    }
    public virtual void Update(object obj)
    {
        _session.Update(obj);
    }
    public virtual void Delete(object obj)
    {
        _session.Delete(obj);
    }
    public virtual object GetById(Type objType, object objId)
    {
        return _session.Load(objType, objId);
    }
    public virtual IQueryable<TEntity> GetAll<TEntity>()
    {
        return _session.Query<TEntity>();
    }
    public void Dispose()
    {
        if (_transaction != null)
        {
            CommitTransaction();
        }
        if (_session != null)
        {
            _session.Flush(); 
            CloseSession();
        }
    }
}