﻿using Coding_Challenge.Entities;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;

public static class Database

{
    public static ISession OpenSession()

    {
        ISessionFactory sessionFactory = Fluently.Configure()
            .Database(MsSqlConfiguration.MsSql2012.ConnectionString(x => x.FromConnectionStringWithKey("VoobanCodingChallenge")))
            .Mappings(m => m.FluentMappings.AddFromAssemblyOf<City>())
            .ExposeConfiguration(config => new SchemaExport(config))
            .BuildSessionFactory();

        return sessionFactory.OpenSession();
    }
}