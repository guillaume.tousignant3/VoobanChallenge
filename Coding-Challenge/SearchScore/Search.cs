﻿using Coding_Challenge.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Coding_Challenge.SearchScore
{
    public class Search
    {
        public Task<List<City>> SearchCity(IEnumerable<City> Cities, string q, double Lat, double Long)
        {
            List<City> _cities = new List<City>();

            foreach (City city in Cities)
            {
                city.Score = (Scorer.LevenshteinToPourcentage(city.Name.Trim().ToLower(), q.Trim().ToLower()));
                if (Lat != 0 || Long != 0 && city.Score > 0.30) 
                {
                    //On ne calcule pas de bonus ni de pénalité si la recherche n'est pas proche de la ville.
                    city.Score = city.Score - Scorer.DistanceBonusPenality(city.Lat, city.Long, Lat, Long);
                }
                if (city.Score >= 0.65)
                {
                    _cities.Add(city);
                }
            }
            //Trier par score descendant
            _cities.Sort((x, y) => y.Score.CompareTo(x.Score)); 
            return Task.FromResult(_cities);
        }
    }
}