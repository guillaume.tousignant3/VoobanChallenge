﻿using System;

namespace Coding_Challenge.SearchScore
{
    public static class Scorer
    {
        /// <summary>Retourne une valeur entre 0 et 1 qui est calculer avec l'algo de Levenshtein.</summary>
        /// <param name="source"></param>
        /// <param name="q"></param>
        /// <returns>Retourne un double.</returns>
        public static double LevenshteinToPourcentage(string source, string q)
        {
            if ((source == null) || (q == null)) return 0.0;
            if (source == q) return 1.0;

            double maxLenght = Math.Max(source.Length, q.Length);
            int stepsToSame = LevenshteinDistance(source, q);

            double score = ((maxLenght - stepsToSame) / maxLenght) * 1;

            // Donner un bonus pour les villes avec moins de caractère et une recherche avec peu de caractère parce que désavantagé par l'algo de Levenshtein.
            if (source.Length <= 4 && q.Length <= 2) 
            {
                if (source.StartsWith(q)) score = score * 1.60;
            }
            //Donner un bonus pour les villes avec plus de caractère pour avoir des suggestions avec le début de la ville.
            else if (source.Length < 7 && source.Length > 4 && q.Length >= 3) 
            {
                if (source.StartsWith(q)) score = score * 1.60;
                else if (source.EndsWith(q)) score = score * 1.20;
                else if (source.Contains(q)) score = score * 1.10;
            }
            //Donner un bonus aux villes plus longue lorsqu'un certain nombre de caractère son entré.
            else if (source.Length >= 7 && q.Length >= 3) 
            {
                if (source.StartsWith(q)) score = score * 1.80;
                else if (source.EndsWith(q)) score = score * 1.40;
                else if (source.Contains(q)) score = score * 1.20;
            }

            if (score > 1) score = 0.99;

            return score;
        }
        /// <summary>Retourne une pénalité ou un bonus si les coordonées sont proches de la ville ou pas. </summary>
        /// <param name="sourceLat"></param>
        /// <param name="sourceLong"></param>
        /// <param name="qLat"></param>
        /// <param name="qLong"></param>
        /// <returns>Retourne un double.</returns>
        public static double DistanceBonusPenality(double sourceLat, double sourceLong, double qLat, double qLong)
        {
            var maxPenalty = 0.30;

            if (IsLatitude(qLat) && IsLongitude(qLong))
            {
                var bonus = 0.0;
                double penalty = 0;
                var distance = getDistanceFromLatLongInKm(sourceLat, sourceLong, qLat, qLong);

                if (distance > 30)
                {
                    //Pénalité de 0.30 pour 1000km de différence et de x.xx pour moins de distance.
                    penalty = (distance * maxPenalty) / 1000; 
                }
                else if (distance <= 30) 
                {
                    //Bonus de 0.10 si les coordonnées sont proche.
                    bonus = 0.10;
                }
                if (penalty > 0.30) penalty = 0.30;

                return penalty - bonus;
            }
            return maxPenalty;
        }
        /// <summary>Retourne la distance en Km entre deux coordonées géographique</summary>
        /// <param name="sourceLat"></param>
        /// <param name="sourceLong"></param>
        /// <param name="qLat"></param>
        /// <param name="qLong"></param>
        /// <returns>Retourne un double.</returns>
        private static double getDistanceFromLatLongInKm(double sourceLat, double sourceLong, double qLat, double qLong) // Traduit en C# de la source suivante en javascript : http://www.movable-type.co.uk/scripts/latlong.html
        {
            var radius = 6371; // Rayon de la terre en km
            var dLat = DegreeToRad(qLat - sourceLat);
            var dLon = DegreeToRad(qLong - sourceLong);
            var a =
              Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
              Math.Cos(DegreeToRad(sourceLat)) * Math.Cos(DegreeToRad(qLat)) *
              Math.Sin(dLon / 2) * Math.Sin(dLon / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = radius * c; // Distance en km
            return d;
        }
        /// <summary>Vérifie si la latidude est valide</summary>
        /// <param name="Lat"></param>
        /// <returns>Retourne un bool.</returns>
        private static bool IsLatitude(double Lat)
        {
            if (Lat >= -90 && Lat <= 90) return true;
            return false;
        }
        /// <summary>Vérifie si la longitude est valide</summary>
        /// <param name="Long"></param>
        /// <returns>Retourne un bool.</returns>
        private static bool IsLongitude(double Long)
        {
            if (Long >= -180 && Long <= 180) return true;
            return false;
        }
        /// <summary>Converti un angle en degrés en radian</summary>
        /// <param name="angle"></param>
        /// <returns>Retourne un double</returns>
        private static double DegreeToRad(double angle)
        {
            return (Math.PI * angle) / 180;
        }
        /// <summary>Calcule le nombre de "steps" nécessaire pour que deux strings deviennent identique. Ex : "allo" et "akko" va retourner 2.</summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        /// <returns>Retourne un int</returns>
        private static int LevenshteinDistance(string source, string target) // Source : https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#C.23
        {
            if (String.IsNullOrEmpty(source))
            {
                if (String.IsNullOrEmpty(target)) return 0;
                return target.Length;
            }
            if (String.IsNullOrEmpty(target)) return source.Length;

            if (source.Length > target.Length)
            {
                var temp = target;
                target = source;
                source = temp;
            }

            var m = target.Length;
            var n = source.Length;
            var distance = new int[2, m + 1];
            // Initialize the distance 'matrix'
            for (var j = 1; j <= m; j++) distance[0, j] = j;

            var currentRow = 0;
            for (var i = 1; i <= n; ++i)
            {
                currentRow = i & 1;
                distance[currentRow, 0] = i;
                var previousRow = currentRow ^ 1;
                for (var j = 1; j <= m; j++)
                {
                    var cost = (target[j - 1] == source[i - 1] ? 0 : 1);
                    distance[currentRow, j] = Math.Min(Math.Min(
                                distance[previousRow, j] + 1,
                                distance[currentRow, j - 1] + 1),
                                distance[previousRow, j - 1] + cost);
                }
            }
            return distance[currentRow, m];
        }
    }
}