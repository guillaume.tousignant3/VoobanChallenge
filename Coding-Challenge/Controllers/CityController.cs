﻿using Coding_Challenge.Entities;
using Coding_Challenge.SearchScore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace Coding_Challenge.Controllers
{
    public class CityController : ApiController
    {
        [HttpGet]
        [Route("suggestions")]
        [ResponseType(typeof(List<City>))]
        public async Task<IHttpActionResult> GetCitiesAsync(string q, double Lat = 0, double Long = 0)
        {
            using (Repository repository = new Repository())
            {
                Search search = new Search();
                
                var cities = repository.GetAll<City>().AsEnumerable();
    
                var matchCities = await search.SearchCity(cities, q, Lat, Long);

                if (matchCities.Any())
                {
                    //Ajouter un entete "Suggestions" à la réponse.
                    return Ok(new { Suggestions = matchCities }); 
                }
                else
                {
                    return NotFound();
                }
            }
        }
    }
}